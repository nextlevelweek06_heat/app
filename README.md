# References

### Documentations

- [Authentication Expo Github](https://docs.expo.dev/guides/authentication/#github)

### Dependencies

- [Moti](https://moti.fyi/): For animation in react native

  - [React Native Reanimated](https://docs.expo.dev/versions/latest/sdk/reanimated/): dependencie of Moti

- [Expo AntDesign Icons](https://icons.expo.fyi/): expo icons library

- [AuthSession](https://docs.expo.dev/versions/latest/sdk/auth-session/): auth session with redirect on mobile

- [AsyncStorage](https://docs.expo.dev/versions/latest/sdk/async-storage/): works like web localStorage

- [Socket IO Client](socket.io/docs/v4/client-api/): to link (publish and listen) messages between backend and frontend

### UI

- [UI Faces Example](uifaces.co/browse-avatars/): Some avatar faces to use for tests
