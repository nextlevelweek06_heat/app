import React from "react";
import { View, Text, TouchableOpacity } from "react-native";

import { styles } from "./styles";

import LogoSvg from "../../assets/logo.svg";
import UserPhoto from "../UserPhoto";
import { useAuth } from "../../hooks/auth";

const Header = () => {
  const { user, signOut } = useAuth();
  return (
    <View style={styles.container}>
      <LogoSvg />

      {user && (
        <>
          <View style={styles.logoutButton}>
            <TouchableOpacity onPress={signOut}>
              <Text style={styles.logoutText}>Sair</Text>
            </TouchableOpacity>
          </View>

          <UserPhoto imageUri={user?.avatar_url} />
        </>
      )}
    </View>
  );
};

export default Header;
