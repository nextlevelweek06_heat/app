import { StyleSheet } from "react-native";
import { getSpacing, TYPOGRAPHY } from "../../theme";

export const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: getSpacing(),
  },
  logoutButton: {
    flexDirection: "row",
    alignItems: "center",
  },
  logoutText: {
    ...TYPOGRAPHY.NORMAL,
    marginRight: getSpacing(),
  },
});
