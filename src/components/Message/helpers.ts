import { PHOTOS } from "../../utils/defaultUrls";
import { Message } from "../../interfaces";

export const mockMessage: Message = {
  id: "1",
  text: "mensagem de teste",
  user: {
    name: "José",
    avatar_url: PHOTOS.MEN01,
  },
};
