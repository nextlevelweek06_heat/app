import React from "react";
import { View, Text } from "react-native";
import { MotiView } from "moti";

import { styles } from "./styles";

import UserPhoto from "../UserPhoto";
import { MessageProps } from "./interfaces";

const Message = ({ data }: MessageProps) => {
  return (
    <MotiView
      from={{ opacity: 0, translateY: -50 }}
      animate={{ opacity: 1, translateY: 0 }}
      transition={{ type: "timing", duration: 700 }}
      style={styles.container}
    >
      <Text style={styles.message}>{data.text}</Text>

      <View>
        <UserPhoto imageUri={data.user.avatar_url} size="SMALL" />

        <Text style={styles.userName}>{data.user.name}</Text>
      </View>
    </MotiView>
  );
};

export default Message;
