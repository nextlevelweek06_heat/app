import { IMessage } from "../../interfaces";

export type MessageProps = {
  data: IMessage;
};
