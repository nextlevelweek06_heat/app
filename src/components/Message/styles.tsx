import { StyleSheet } from "react-native";
import { getSpacing, TYPOGRAPHY } from "../../theme";

export const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginBottom: getSpacing(2),
  },
  message: {
    ...TYPOGRAPHY.NORMAL,
    lineHeight: 20,
    marginBottom: getSpacing(),
  },
  footer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
  },
  userName: {
    ...TYPOGRAPHY.NORMAL,
    marginLeft: getSpacing(),
  },
});
