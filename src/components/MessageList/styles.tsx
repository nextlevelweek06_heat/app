import { StyleSheet } from "react-native";
import { getSpacing, TYPOGRAPHY } from "../../theme";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: getSpacing(2),
  },
  content: {
    paddingTop: 135,
    paddingBottom: 184,
  },
});
