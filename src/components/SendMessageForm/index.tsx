import React, { useState } from "react";
import { Alert, Keyboard, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { api } from "../../services/api";
import { COLORS } from "../../theme";
import Button from "../Button";

import { styles } from "./styles";

const SendMessageForm = () => {
  const [message, setMessage] = useState("");
  const [sendingMessage, setSendingMessage] = useState(false);

  const handleMessageSubmit = async () => {
    const messageFormatted = message.trim();

    if (messageFormatted.length > 0) {
      setSendingMessage(true);

      try {
        await api.post("/message", { message: messageFormatted });

        setMessage("");
        Keyboard.dismiss();
        setSendingMessage(false);
        Alert.alert("Mensagem enviada com sucesso!");
      } catch (error) {
        Alert.alert(
          "Ocorreu algum problema no envio da mensagem, por favor tente novamente..."
        );
        console.error({ error, code: "TRY_SEND_MESSAGE--0000" });
      }
    } else {
      Alert.alert("Escreva a mensagem para enviar...");
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        keyboardAppearance="dark"
        placeholder="Qual sua expectativa para o evento?"
        placeholderTextColor={COLORS.GRAY_PRIMARY}
        multiline
        maxLength={140}
        editable={!sendingMessage}
        style={styles.input}
        value={message}
        onChangeText={setMessage}
      />

      <Button
        title="ENVIAR MENSAGEM"
        backgroundColor={COLORS.PINK}
        color={COLORS.WHITE}
        isLoading={sendingMessage}
        onPress={handleMessageSubmit}
      />
    </View>
  );
};

export default SendMessageForm;
