import React from "react";
import { Image } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { SIZES } from "./helpers";
import { UserPhotoProps } from "./interfaces";

import { styles } from "./styles";
import avatarImg from "../../assets/user.png";
import { COLORS } from "../../theme";

const AVATAR_DEFAULT = Image.resolveAssetSource(avatarImg).uri;

const UserPhoto = ({
  imageUri = AVATAR_DEFAULT,
  size = "NORMAL",
}: UserPhotoProps) => {
  const { avatarSize, containerSize } = SIZES[size];

  return (
    <LinearGradient
      colors={[COLORS.PINK, COLORS.YELLOW]}
      start={{ x: 0, y: 0.8 }}
      end={{ x: 0.9, y: 1 }}
      style={[
        styles.container,
        {
          width: containerSize,
          height: containerSize,

          borderRadius: containerSize / 2,
        },
      ]}
    >
      <Image
        source={{ uri: imageUri }}
        style={[
          styles.avatar,
          {
            width: avatarSize,
            height: avatarSize,

            borderRadius: avatarSize / 2,
          },
        ]}
      />
    </LinearGradient>
  );
};

export default UserPhoto;
