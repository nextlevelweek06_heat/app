import { SIZES } from "./helpers";

export interface UserPhotoProps {
  imageUri?: string;
  size?: keyof typeof SIZES;
}
