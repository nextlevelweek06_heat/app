import React, { createContext, useContext, useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as AuthSessions from "expo-auth-session";

import { getStorage, removeStorage, setStorage } from "../../utils/storage";
import { api, setApiHeaderToken } from "../../services/api";
import { User } from "../../interfaces";
import {
  AuthContextData,
  AuthorizationResponse,
  AuthProviderProps,
  AuthResponse,
} from "./interfaces";

export const AuthContext = createContext({} as AuthContextData);

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const [isSigning, setIsSigning] = useState(true);
  const [user, setUser] = useState<User | null>(null);

  const signIn = async () => {
    try {
      setIsSigning(true);
      const CLIENT_ID = "bb4ed725f61c47faa459";
      const SCOPE = "read:user";

      const authUrl = `https://github.com/login/oauth/authorize?client_id${CLIENT_ID}&scope=${SCOPE}`;

      const authSessionResponse = (await AuthSessions.startAsync({
        authUrl,
      })) as AuthorizationResponse;

      if (
        authSessionResponse.type === "success" &&
        authSessionResponse.params?.error !== "access_denied"
      ) {
        const authResponse = await api.post("/authenticate", {
          code: authSessionResponse.params.code,
        });
        const { user, token } = authResponse.data as AuthResponse;

        setApiHeaderToken(token);

        setStorage({
          USER: JSON.stringify(user),
          TOKEN: token,
        });

        setUser(user);
      }
    } catch (error) {
      console.error({ error, code: "TRY_SIGNIN--0000" });
    } finally {
      setIsSigning(false);
    }
  };

  const signOut = async () => {
    setUser(null);
    removeStorage();
  };

  useEffect(() => {
    const loadUserStorageData = async () => {
      const { USER, TOKEN } = await getStorage();

      if (USER && TOKEN) {
        setApiHeaderToken(TOKEN);
        setUser(JSON.parse(USER));
      }

      setIsSigning(false);
    };

    loadUserStorageData();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
        isSigning,
        signIn,
        signOut,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  return context;
};
