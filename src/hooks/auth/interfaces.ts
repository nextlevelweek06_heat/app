import React from "react";
import { User } from "../../interfaces";

export type AuthContextData = {
  user: User | null;
  isSigning: boolean;
  signIn: () => Promise<void>;
  signOut: () => Promise<void>;
};

export type AuthProviderProps = {
  children: React.ReactNode;
};

export type AuthResponse = {
  token: string;
  user: User;
};

export type AuthorizationResponse = {
  params: {
    code?: string;
    error?: string;
  };
  type?: string;
};
