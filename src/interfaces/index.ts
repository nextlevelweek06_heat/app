export interface IMessage {
  id: string;
  text: string;
  user: MessageUser;
}

export interface User {
  id: string;
  name: string;
  avatar_url: string;
  login: string;
}

export type MessageUser = Omit<User, "id" | "login">;
