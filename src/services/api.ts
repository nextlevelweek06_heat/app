import axios from "axios";

export const api = axios.create({
  // TODO: quando em modo de desenvolvimento usar o local:
  // http://189.121.201.138:4000
  baseURL: "http://189.121.201.138:4000",
});

export const setApiHeaderToken = (token: string) =>
  (api.defaults.headers.common["Authorization"] = `Bearer ${token}`);
