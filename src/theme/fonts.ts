import { COLORS } from "./colors";

export const FONTS = {
  REGULAR: "Roboto_400regular",
  BOLD: "Roboto_700Bold",
};

export const TYPOGRAPHY = {
  NORMAL: {
    fontSize: 15,
    fontFamily: FONTS.REGULAR,
    color: COLORS.WHITE,
  },
};
