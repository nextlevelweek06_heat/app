const SPACING = 16;

export const getSpacing = (value = 1) => SPACING * value;
