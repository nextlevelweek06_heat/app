export const photoImageSrc =
  "https://media-exp1.licdn.com/dms/image/C4D03AQEQTkST_F7UmA/profile-displayphoto-shrink_400_400/0/1615460085142?e=1646870400&v=beta&t=BWoqclxx85ax5tCAX8TJxcLMvQl4SSlnVz8GSpM-Vq4";

// REFERENCE URLS
// https://www.uifaces.co/browse-avatars/
export const PHOTOS = {
  MEN01: "https://randomuser.me/api/portraits/men/97.jpg",
  MEN02: "https://randomuser.me/api/portraits/men/12.jpg",
  WOMEN01: "https://api.uifaces.co/our-content/donated/3799Ffxy.jpeg",
  WOMEN02:
    "https://images.unsplash.com/photo-1496081081095-d32308dd6206?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=dd302358c7e18c27c4086e97caf85781",
};
