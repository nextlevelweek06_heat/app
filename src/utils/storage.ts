import AsyncStorage from "@react-native-async-storage/async-storage";

const SUFIX_NAME = "@nlwheat";

const getStorageName = (name: string) => `${SUFIX_NAME}:${name}`;

export const STORAGE = {
  USER: getStorageName("user"),
  TOKEN: getStorageName("token"),
};

interface IStorage {
  USER: string;
  TOKEN: string;
}

type StorageKeys = keyof typeof STORAGE;

const storageKeysList = Object.keys(STORAGE) as StorageKeys[];

export const getStorage = async (
  keys = storageKeysList
): Promise<Partial<IStorage>> => {
  return await Promise.all(
    keys.map(async (key) => {
      const typedKey = key as StorageKeys;
      await AsyncStorage.getItem(STORAGE[typedKey]);
    })
  ).then((storage) => storage as Partial<IStorage>);
};

export const setStorage = async (values: Partial<IStorage>): Promise<void> => {
  await Promise.all(
    Object.entries(values).map(async ([key, value]) => {
      const typedKey = key as StorageKeys;
      await AsyncStorage.setItem(STORAGE[typedKey], JSON.stringify(value));
    })
  );
};

export const removeStorage = async (keys = storageKeysList): Promise<void> => {
  await Promise.all(
    keys.map(async (key) => {
      const typedKey = key as StorageKeys;
      await AsyncStorage.removeItem(STORAGE[typedKey]);
    })
  );
};
